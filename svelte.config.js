import adapter from '@sveltejs/adapter-node';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: preprocess({
		scss: {
		  includePaths: ['src'],
		  prependData: "@import 'src/styles/variables';"
		},
		sourceMap: true
	  }),

	kit: {
		adapter: adapter(),
		alias: {
			$components: './src/components',
			$types: './src/types',
			$src: './src',
			$lib: './src/lib'
		},
		csrf: {
		  checkOrigin: false
		}
	}
};

export default config;
