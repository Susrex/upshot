FROM node:18 AS frontend

COPY package.json package-lock.json /app/
WORKDIR /app
# Install dependencies
RUN npm ci
# Build the app
COPY *config* /app/
COPY prisma /app/prisma
RUN npx prisma generate

COPY static /app/static
COPY src /app/src

RUN npm run build

ENV PORT=80
EXPOSE 80

CMD [ "sh", "-c", "npx prisma migrate deploy; node build" ]
