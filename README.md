![Ilustrative img header](/static/images/imgHeader.png)
# Up$hot

Toto je maturitní projekt vytvořen pro Gymnázium Nad Alejí ve školním roce 2022/2023

Účelem tohoto projektu bylo vytvořit webovou aplikaci, nástroj na spravování financí. Jeho hlavními vlastnostmi je možnost importovat transakce z .csv souboru (takový soubor lze lehce stáhnout z internetového bankovncitví) a automatické třídění takto naimportovaných financí podle jejich účelu. Data z takto vytříděných transakcí mohou být zobrazena v grafech.

- Živé demo tohoto projektu najdete na stránce:[https://upshot.gpcz.eu](https://upshot.gpcz.eu) (děkuji za zřízení hostování [Ondřej Maxa](https://gitlab.com/majksa))
- Návod jak aplikaci používat najdete zde: [uživatelská dokumentace](https://gitlab.com/Susrex/upshot/-/blob/main/UpShotPaper.pdf)
- Pokud vás zajímají techničtější detaily pokračujte ve čtení

## Developer documentation

The code is thoroughly docummented with comments

### How to build the app locally

- requirements: postgreSQL, docker installed on your machine

Quick step by step guide:

1. Clone this repo
2. For initial setup:
   ```
   npm install
   echo 'DATABASE_URL="postgresql://admin:admin@localhost:5432/upshotdb?schema=public"' > .env
   docker compose up -d
   npx prisma generate
   ```
3. To start the app run:
   ```
   docker compose up -d
   npm run dev
   ```

Commands explained:
- `npm install` installs the dependencies
- `echo 'DATABASE_URL="postgresql://admin:admin@localhost:5432/upshotdb?schema=public"' > .env` creates .env file and saves the text: 'DATABASE_URL="postgresql://admin:admin@localhost:5432/upshotdb?schema=public"' into the file, you can change the values if you change the docker-comopose.yml respectively
- `docker compose up` executes `docker-compose.yml` which creates container containing the database
- `npx prisma generate` creates the SQL database out of prisma schema
- `npm run dev` starts node development server

### Database explained

This web app uses postgreSQL database running in a docker container.

There are 7 different tables (you can see the [scheme](prisma/schema.prisma) for more details)

- User
- Transaction
- Tag
- Account
- Session
- TagOnTransaction
  - pivot table handling n-to-n realtion of tag and transaction tables
- TagOnAccount
  - pivot table handling n-to-n realtion of tag and account tables

See this scheme which describes the relations between tables:
```mermaid
flowchart TB
	A[users]
	B[transactions]
	C[tags]
	F[tagOnAccount]
	D[accounts]
	E[tagOnTransaction]
  G[sessions]

	A -->| 1 to n |B
	B <--> |n to n|C
	A --> | 1 to n |C[tags]
	C <--> |n to n|D
	C --> F
	A --> |1 to n| D[accounts]
	B --> E[tagOnTransaction]
	C --> E
	D --> F[tagOnAccount]
  A --> | 1 to n| G
```
