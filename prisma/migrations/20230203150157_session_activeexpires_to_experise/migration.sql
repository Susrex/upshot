/*
  Warnings:

  - You are about to drop the column `active_expires` on the `session` table. All the data in the column will be lost.
  - Added the required column `expires` to the `session` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "session" DROP COLUMN "active_expires",
ADD COLUMN     "expires" BIGINT NOT NULL;
