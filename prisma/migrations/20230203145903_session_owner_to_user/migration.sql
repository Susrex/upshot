/*
  Warnings:

  - You are about to drop the column `expires` on the `session` table. All the data in the column will be lost.
  - You are about to drop the column `owner_id` on the `session` table. All the data in the column will be lost.
  - Added the required column `active_expires` to the `session` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_id` to the `session` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "session" DROP CONSTRAINT "session_owner_id_fkey";

-- DropIndex
DROP INDEX "session_owner_id_idx";

-- AlterTable
ALTER TABLE "session" DROP COLUMN "expires",
DROP COLUMN "owner_id",
ADD COLUMN     "active_expires" BIGINT NOT NULL,
ADD COLUMN     "user_id" TEXT NOT NULL;

-- CreateIndex
CREATE INDEX "session_user_id_idx" ON "session"("user_id");

-- AddForeignKey
ALTER TABLE "session" ADD CONSTRAINT "session_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;
