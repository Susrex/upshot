/*
  Warnings:

  - You are about to drop the column `public` on the `TagOnAccount` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "TagOnAccount" DROP COLUMN "public";
