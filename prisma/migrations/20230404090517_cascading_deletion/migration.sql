-- DropForeignKey
ALTER TABLE "Tag" DROP CONSTRAINT "Tag_owner_id_fkey";

-- DropForeignKey
ALTER TABLE "TagOnTransaction" DROP CONSTRAINT "TagOnTransaction_tagId_fkey";

-- DropForeignKey
ALTER TABLE "TagOnTransaction" DROP CONSTRAINT "TagOnTransaction_transactionId_fkey";

-- AddForeignKey
ALTER TABLE "Tag" ADD CONSTRAINT "Tag_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TagOnTransaction" ADD CONSTRAINT "TagOnTransaction_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TagOnTransaction" ADD CONSTRAINT "TagOnTransaction_transactionId_fkey" FOREIGN KEY ("transactionId") REFERENCES "Transaction"("id") ON DELETE CASCADE ON UPDATE CASCADE;
