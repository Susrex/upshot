-- DropForeignKey
ALTER TABLE "TagOnAccount" DROP CONSTRAINT "TagOnAccount_accountId_fkey";

-- DropForeignKey
ALTER TABLE "TagOnAccount" DROP CONSTRAINT "TagOnAccount_tagId_fkey";

-- AddForeignKey
ALTER TABLE "TagOnAccount" ADD CONSTRAINT "TagOnAccount_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TagOnAccount" ADD CONSTRAINT "TagOnAccount_accountId_fkey" FOREIGN KEY ("accountId") REFERENCES "Account"("id") ON DELETE CASCADE ON UPDATE CASCADE;
