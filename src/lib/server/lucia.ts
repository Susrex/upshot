/********************************************
 * Copied from: https://lucia-auth.com/start-here/getting-started?framework=sveltekit
 *******************************************/
import lucia from 'lucia-auth';
import prisma from '@lucia-auth/adapter-prisma';
import { dev } from '$app/environment';
import { PrismaClient } from '@prisma/client';

const prismaClient = new PrismaClient();

/**
 * Initializing lucia, used for user authentification
 */
export const auth = lucia({
  adapter: prisma(prismaClient),
  env: dev ? 'DEV' : 'PROD',
  transformUserData: (userData) => {
    return {
      userId: userData.id,
      username: userData.username
    };
  }
});

//this moduel and th file that holds it should NOT be Imported from the client
export type Auth = typeof auth;
