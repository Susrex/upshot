/**
 * Array of accounts to import to each user upon user registration
 */
export const defaultAccounts = [
  // These are existing accounts
  { number: '188505042/0300', name: 'Alza CSOB', tagNames: 'shopping,technology' },
  { number: '1265098001/5500', name: 'Alza Raiffeisenbank', tagNames: 'shopping,technology' },
  { number: '2064330104/2600', name: 'Mall.cz', tagNames: 'shopping,technology' },
  { number: '43-2324190217/0100', name: 'Primirest', tagNames: 'food' },
  { number: '19-8057160247/0100', name:'czc.cz KB', tagNames: 'shopping,electronics'}
];
