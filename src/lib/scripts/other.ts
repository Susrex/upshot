/**
 * Converts czech date string into date
 * @param stringDate - string of date writen in czech format: day.month.year for example: '2.3.2022'
 * @returns date
 */
export function stringToDate(stringDate: string): Date {
  try {
    const dateMembers = stringDate.split('.');
    const year = parseInt(dateMembers[2]);
    const month = parseInt(dateMembers[1]) - 1;
    const day = parseInt(dateMembers[0]);
    return new Date(year, month, day);
  } catch (e) {
    throw new Error('Could not convert to date');
  }
}
// Comparing two TagObject[] returns all items which are in first but not in second array
type CompareTagsFunction = (a: TagObject, b: TagObject) => boolean;
/**
 * Descriebs how to compare two TagObjects
 * @param a - first tag
 * @param b - second tag
 * @returns - true if their ids are the same, false otherwise
 */
const isSameTag:CompareTagsFunction = (a: TagObject, b: TagObject) => a.id === b.id;
/**
 * Compares two tag arrays returns an array of tags which are included in the first but not in the second
 * @param a - first tag array
 * @param b - second tag array
 * @param compareFunction - function which describes how to compare TagObjects
 * @returns array of tags which are included in the first but not in the second
 */
const onlyInLeft = (a: TagObject[], b: TagObject[], compareFunction: CompareTagsFunction) =>
  a.filter((c: TagObject) => !b.some((d: TagObject) => compareFunction(c, d)));
/**
 * Uses onlyInLeft to compare two tags array, tags are compared using isSameTag
 * @param array1 - first tag array
 * @param array2 - second tag array
 * @returns array of tags which are included in the first but not in the second
 */
export function compareTagsArray(array1: TagObject[], array2: TagObject[]) {
  return onlyInLeft(array1, array2, isSameTag);
}

export interface Form {
  error: boolean;
  visible: boolean;
  message: string;
  username: string;
}

export interface TransactionObject {
  id: number;
  account_number: string;
  due_date: Date;
  amount: number;
  currency: string;
  balance: number;
  counter_account: string;
  counter_account_bank_code?: string;
  counter_account_name?: string;
  constant_symbol?: string;
  variable_symbol?: string;
  specific_symbol?: string;
  transaction_type?: string;
  note?: string;
  original_transaction?: string;
  rate?: string;
  E2E_Identification?: string;
  original_payer?: string;
  the_final_beneficiary?: string;
  owner_id?: string;
  tags?: TagObject[];
}

export interface TagObject {
  id: number;
  name: string;
  color?: string;
  tagId?: number;
}

export interface AccountObject {
  id?: number;
  owner_id?: string;
  name: string;
  number: string;
  tags?: TagObject[];
  tagNames?: string;
}
