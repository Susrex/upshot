/**
 * Converts file into js objects
 *  - source: https://stackoverflow.com/a/64396703
 * @param file The csv file to export objects from
 * @param header True if the first non empty line doesn't contain object[keys] - object[keys] are on the third line and the first two lines are skipped
 * @param valueSeparator The symbol which seperates .csv cells
 * @returns array of transactions
 */
export async function CSVtoJson(file: File, header: boolean = true, valueSeparator: string = ';') {
  const data = await file.text();
  let title, empty, headerLine, lines;
  if (header) {
    // Split data into lines and separate headers from actual data
    // using Array spread operator
    [title, empty, headerLine, ...lines] = data.split('\n');
  } else {
    [headerLine, ...lines] = data.split('\n');
  }
  // Split headers line into an array
  if (!headerLine) {
    return {};
  }
  const headers = headerLine.split(valueSeparator);
  // Create objects from parsing lines
  if (!lines) {
    return {};
  }
  const parsedObjects = lines.map((line, index) =>
    line
      // Split line with value separators
      .split(valueSeparator)
      // Reduce values array into an object like: { [header]: value }
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
      .reduce(
        // Reducer callback
        (object, value, index) => ({
          ...object,
          [headers[index]]: value
        }),
        // Initial value (empty JS object)
        {}
      )
  );
  return parsedObjects;
}

/**
 * Converts object array into csv string
 *  - source: https://stackoverflow.com/a/22792982
 * @param objArray array of objects to convert to .csv string
 * @param valueSeparator The symbol which seperates .csv cells
 * @returns .csv string
 */
export function convertToCSV(objArray: any[], valueSeparator: string = ';') {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  if (objArray.length <= 0) return '';

  // creates column headers
  let keys = Object.keys(objArray[0]);
  let result = keys.join(valueSeparator) + '\r\n';

  // Add the rows
  array.forEach((line) => {
    result += keys.map((k) => line[k]).join(valueSeparator) + '\r\n';
  });
  return result;
}
