import * as bcrypt from 'bcrypt';

// Usage:
//  const hashWord = await Encrypt.cryptPassword(password);
//  const passwordMatches = await Encrypt.comparePassword(password, passwordHash);

export const Encrypt = {
  /**
   * Uses bcrypt to hash and salt given strings
   * @param password - string to hash
   * @returns hashed and salted string
   */
  cryptPassword: (password: string) =>
    bcrypt
      .genSalt(10)
      .then((salt) => bcrypt.hash(password, salt))
      .then((hash) => hash),
  /**
   * Uses bcrypt.compare to compare hashed and unhashed passwords
   * @param password - unecrypted string
   * @param hashPassword - encrypted string
   * @returns true if passwords are the same false otherwise
   */
  comparePassword: (password: string, hashPassword: string) =>
    bcrypt.compare(password, hashPassword).then((resp) => resp)
};
