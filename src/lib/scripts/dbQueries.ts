import { db } from '$lib/scripts/db';
import type { AccountObject } from '$lib/scripts/other';

/**
 * This function makes db queries to get an array of current user's accounts with tags assigned to them
 * @param userId id of the current user
 * @returns array of accounts with property tags (array of tags assigned to the acccount)
 */
export async function getAccountsWithTags(userId: string) {
  let accounts = await db.getAccountsByUserId(userId);
  let tagPromises: any[] = [];
  // getting account tags
  accounts.forEach((account) => {
    tagPromises.push(db.getTagsByAccountId(account.id));
  });
  return Promise.all(tagPromises).then((tagsArray) => {
    // saving tags into accounts array
    for (let i = 0; i < accounts.length; i++) {
      accounts[i].tags = tagsArray[i];
    }
    return accounts;
  });
}

/**
 * This functions finds if any account name matches the given name
 * @param accounts array of account objects to find matching name in
 * @param name name to find
 * @returns -1 or index of first account whose name matches the given name
 */
export function getMatchingAccountName(accounts: AccountObject[], name: string) {
  return accounts.map((e) => e.name.toUpperCase()).indexOf(name.toUpperCase());
}

/**
 * This functions finds if any account number matches the given number
 * @param accounts array of account objects to find matching number in
 * @param name number to find
 * @returns -1 or index of first account whose number matches the given number
 */
export function getMatchingAccountNumber(accounts: AccountObject[], number: string) {
  return accounts.map((e) => e.number.toUpperCase()).indexOf(number.toUpperCase());
}
