import type { TransactionObject, TagObject } from '$src/lib/scripts/other';

/**
 * Functions takes in all transaction and makes sum of all incomes and of all expenses
 * @param data transactions to extract info from
 * @returns 2 item array: [sum of all incomes, sum of all expenses] (in absolute value)
 */
function IncomeExpenseRatio(data: TransactionObject[]) {
  const amounts = data.map(({ amount }) => amount);
  let incomeSum = 0;
  let expenseSum = 0;
  amounts.forEach((element) => {
    if (element && element > 0) {
      incomeSum += element;
    } else if (element && element < 0) {
      expenseSum += element;
    }
  });
  return [Math.abs(incomeSum), Math.abs(expenseSum)];
}

/**
 * Makes two arrays out of array containing two item arrays
 * @param data array of two item arrays
 * @returns array of arrays
 */
function regroupToTwo(data: any[]) {
  let first: number[] = [];
  let second: number[] = [];
  data.forEach((item) => {
    first.push(item[0]);
    second.push(item[1]);
  });
  return [first, second];
}

/**
 * Creates chart data to compare the sum and expenses of each tag
 * @param data array of transactions array - each array contains transactions of single tag
 * @param tags user tags to take names and colors from
 * @returns createRadarData() -> radar chart data
 */
export function tagsIncomeExpenseRatio(data: TransactionObject[][], tags: TagObject[]) {
  let result: number[][] = [];
  data.forEach((transactionArray) => {
    result.push(IncomeExpenseRatio(transactionArray));
  });
  const twoDataSets = regroupToTwo(result);
  const labels = tags.map(({ name }) => name);
  return createRadarData(twoDataSets, labels);
}
/**
 * Creates data structure for radar chart creation
 *  - there are two sets of data one for expenses, the second for incomes
 *  - the data including labels must be in order
 * @param twoDataSets 2 arrays(incomes and expenses) of each tag [[tag1Income, tag2Income...], [tag1Expense, tag2Expense, ...]]
 * @param labels how to label each radar point (name of tags) [tag1Name, tag2Name ...]
 * @returns object to create radar chart from
 */
function createRadarData(twoDataSets: number[][], labels: string[]) {
  return {
    labels: labels,
    datasets: [
      {
        label: 'Income',
        data: twoDataSets[0],
        fill: true,
        backgroundColor: '#00ff001C',
        borderColor: '#00FF00',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: '#00FF00'
      },
      {
        label: 'Expense',
        data: twoDataSets[1],
        fill: true,
        backgroundColor: '#ff00001C',
        borderColor: '#ff0000',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: '#ff0000'
      }
    ]
  };
}

//data
/**
 * Feeds given data to createBarChartData() function
 * @param data array following this structure: [ [tagSum1, color1], [tagSumm2, color2], ...]
 * @param tags array of all user tags -> used for extracting colors and names
 * @returns data for bar chart
 */
export function createTagBalanceData(data: number[], tags: TagObject[]) {
  const labels = tags.map(({ name }) => name);
  const colors = tags.map(({ color }) => color);
  const colorsTransparent = tags.map(({ color }) => color + '3C');
  return createBarChartData(data, labels, colors, colorsTransparent);
}

/**
 * Creates bar chart data object
 * @param data ordered array of values
 * @param labels ordered array of labels
 * @param borderColors ordered array of border colors
 * @param backgroundColors ordered array of background colors
 * @returns bar chart data object
 */
function createBarChartData(
  data: number[],
  labels: string[],
  borderColors: string[] = [],
  backgroundColors: string[] = []
) {
  return {
    labels: labels,
    datasets: [
      {
        label: '',
        data: data,
        backgroundColor: backgroundColors,
        borderColor: borderColors,
        borderWidth: 1,
        maxBarThickness: 150
      }
    ]
  };
}

/**
 * Creates expenses vs incomes doughnut data
 * @param transactions transactions to analyze
 * @returns data object to create doughnut chart
 */
export function createDoughnutData(transactions: TransactionObject[]) {
  const data = IncomeExpenseRatio(transactions);
  return {
    labels: ['incomes', 'expenses'],
    datasets: [
      {
        label: 'Income-Expense ratio',
        data: data,
        backgroundColor: ['#00FF00', '#FF0000'],
        hoverOffset: 4
      }
    ]
  };
}
/**
 * Creates data object for line chart
 * @param transactions transactions to analyze
 * @returns data object for line chart
 */
export function createLineData(transactions: TransactionObject[]) {
  // expects that transactions are sorted in descending order
  let dataObj = sortTransactionsByMonthYear(transactions);
  return {
    labels: dataObj.labels,
    datasets: [
      {
        label: '',
        data: dataObj.data,
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
      }
    ]
  };
}

/**
 * Makes a month-year range starting with @param start date and ending with @param end date
 * @param start start date
 * @param end end date
 * @returns array of strings for example: ['Jun 2022', 'July 2022', 'August 2022']
 */
function getMonthRange(start: Date, end: Date) {
  let startString = getMonthYear(start);
  let startDate = new Date(startString[1], startString[0]);
  let range: Date[] = [];
  while (startDate.getTime() < end.getTime()) {
    range.push(startDate);
    const d = getMonthYear(startDate);
    startDate = new Date(d[1], d[0] + 1);
  }
  return range;
}

/**
 * Extracts month and year out of date
 * @param date - date to get year and month from
 * @returns 2 item array: [month:number, year:number]
 */
function getMonthYear(date: Date) {
  return [date.getMonth(), date.getFullYear()];
}

/**
 * Converts date into month,year string form
 * @param date - date to convert
 * @returns string: month,year for example: 'April,2022' or 'July,2018'
 */
function getMontYearStr(date: Date) {
  let month = date.toLocaleString('default', { month: 'long' });
  let year = date.getFullYear();
  return `${month},${year}`;
}

/**
 * Sorts transactions by month,year of their due_date and sums their amounts
 * @param transactions - transactions to analyze (expects that transactions are sorted from earliest to latest)
 * @returns
 */
function sortTransactionsByMonthYear(transactions: TransactionObject[]) {
  // each transaction get assigned to a key in form of [month],[year]
  let obj = {};
  let range: Date[] = getMonthRange(
    transactions[0].due_date,
    transactions[transactions.length - 1].due_date
  );
  // an obj.key is created for each month,year in the range
  for (const key of range) {
    obj[getMontYearStr(key)] = [];
  }
  // each transaction is assigned under the correct key
  transactions.forEach((transaction) => {
    let stringDate = getMontYearStr(transaction.due_date);
    obj[stringDate].push(transaction);
  });
  return sumOfSorted(obj);
}

/**
 * Sums transactions which are assigned to obj.keys
 * @param obj - sorted transactions under obj.keys
 * @returns an object containing labels (range of month,year) and data (sum of transaction amounts under each obj.key)
 */
function sumOfSorted(obj: object) {
  let data = [];
  const labels = Object.keys(obj);
  for (const key of labels) {
    data.push(
      obj[key].reduce(
        (accumulator: number, transaction: TransactionObject) => accumulator + transaction.amount,
        0
      )
    );
  }
  return { labels: labels, data: data };
}

/**
 * Calculates balance/income/expense by weekday
 *  - transactions are sorted by weekday
 *  - each weekday's transactions are summed
 * @param transactions - transactions to analyze
 * @param limit - specifying if the returned data should be limited (true for yes, false for no)
 * @param expense  - specifying how the data should be limited (only expenses or only incomes)
 * @returns barChartData() -> data for bar chart
 */
export function createWeekdaySpendingData(
  transactions: TransactionObject[],
  limit: boolean,
  expense: boolean
) {
  let obj = {
    Monday: [],
    Tuesday: [],
    Wednesday: [],
    Thursday: [],
    Friday: [],
    Saturday: [],
    Sunday: []
  };
  transactions.forEach((transaction) => {
    if (limit) {
      // weekdays expenses only
      if (expense && transaction.amount < 0) {
        let weekDay = transaction.due_date.toLocaleDateString('default', { weekday: 'long' });
        obj[weekDay].push(transaction);
      }
      // weekdays income only
      if (!expense && transaction.amount > 0) {
        let weekDay = transaction.due_date.toLocaleDateString('default', { weekday: 'long' });
        obj[weekDay].push(transaction);
      }
      // weekday balance
    } else {
      let weekDay = transaction.due_date.toLocaleDateString('default', { weekday: 'long' });
      obj[weekDay].push(transaction);
    }
  });
  let data = sumOfSorted(obj);
  return createBarChartData(data.data, data.labels);
}
