import { db } from '$lib/scripts/db';
import type { TagObject, AccountObject } from '$lib/scripts/other';

/**
 * Imports accounts and tags connected with it into db
 * @param accounts list of accounts imported from file (it is not AccountObject), contain properties: number, name tagNames
 * @param userId - id of the current user
 * @return stats object: stats.tagsCreated (number of created tags), stats.accountsCreted (number of created accounts), stats.tagsAssigned (number of tags assigned), stats.problemsEncountered (amount of times import run into problems)
 */
export async function importAccounts(accounts, userId:string) {
  let stats = {
    tagsCreated: 0,
    accountsCreated: 0,
    accountsFoundInDB: 0,
    tagsAssigned: 0,
    problemsEncountered: 0
  };

  // finds connected tag names of imported accounts
  let tagsImported: string[] = [];
  accounts.forEach((account) => {
    let tags = [];
    if (account['tagNames']) {
      tags = account['tagNames'].split(',');
    }
    account.tags = [];
    tags.forEach((tag: string) => {
      tagsImported.push(tag);
      account.tags.push(tag);
    });
  });
  // filtering array to contain each item only once and removes space
  tagsImported = Array.from(new Set(tagsImported)).filter((item) => item != '');

  // finds or creates tag in db and saves them in dbTags - makes sure all of the imported tags exist in db
  let dbTags: TagObject[] = [];
  for (const tagName of tagsImported) {
    let tag: TagObject;
    tag = await db.getTagByNameAndUserId(tagName, userId);
    // Only tags which do not exist are created again
    if (!tag) {
      tag = await db.createTag({
        name: tagName,
        color: '#ececec',
        owner_id: userId
      });
      stats.tagsCreated += 1;
    }
    dbTags.push(tag);
  }

  // finds/creates account and assignes tags to it
  for (const account of accounts) {
    let dbAccount: AccountObject;
    let tagIdsToConnect: number[] = [];
    dbAccount = await db.getAccountByNameNumberUserId(account.name, account.number, userId);
    if (dbAccount) {
      stats.accountsFoundInDB += 1
      // assignes tags to existing account
      for (const tag of account.tags) {
        // finds the tag id of tag and pushes it into tagIdsToConnect
        let dbTag = dbTags.find((tagObj) => tagObj.name == tag);
        // preventing previously assigned tags to be assigned again
        if (dbTag != undefined && dbAccount.tags.map((tag) => tag.tagId).indexOf(dbTag.id) < 0) {
          tagIdsToConnect.push(dbTag.id);
          stats.tagsAssigned += 1;
        }
      }
      try {
        db.assignTagsToAccount(dbAccount.id, tagIdsToConnect);
      } catch (e) {
        console.log('Assigning tags to existing account failed at import due to:');
        console.log(e);
        stats.problemsEncountered += 1;
      }
    } else {
      console.log('account ', account.name, 'must be created first');
      for (const tag of account.tags) {
        // finding out which tags to assign to the account
        let dbTag = dbTags.find((tagObj) => tagObj.name == tag);
        if (dbTag != undefined) {
          tagIdsToConnect.push(dbTag.id);
          stats.tagsAssigned += 1;
        }
      }
      try {
        db.createAccountWithTags(account.name, account.number, tagIdsToConnect, userId);
        stats.accountsCreated += 1;
      } catch (e) {
        console.log('Creating account with tags failed at import due to:');
        console.log(e);
        stats.problemsEncountered += 1;
      }
    }
  }
  return stats;
}
