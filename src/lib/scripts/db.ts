import { PrismaClient, type User, type Transaction, type Tag, type Account } from '@prisma/client';
import type { TagObject } from '$lib/scripts/other';

const prisma = new PrismaClient();

/**
 * Creates new user
 * @param data User create input (userName, hashedPassword)
 * @returns created user
 */
async function createUser(data: User.UserCreateInput): Promise<User> {
  console.log('Creating user:', data.username);
  return await prisma.user.create({
    data
  });
}
/**
 * Finds user by its username
 * @param username - username of the user
 * @returns user if it has been found
 */
async function getUserByUsername(username: string): Promise<User | null> {
  console.log('Looking for user:', username);
  return await prisma.user.findUnique({
    where: {
      username: username
    }
  });
}
/**
 * Deletes user by its id
 * @param id - id of the user
 * @returns deleted user
 */
async function deleteUser(id: string) {
  console.log('Deleting user:', id);
  return await prisma.user.delete({
    where: {
      id: id
    }
  });
}

/**
 * Creates new transaction
 * @param data Transaction create input (see type TransactionObject for more)
 * @returns newly created transaction
 */
async function createTransaction(data: Transaction.TransactionCreateInput): Promise<Transaction> {
  console.log('Creating transaction:', data.amount);
  return await prisma.transaction.create({
    data
  });
}
/**
 * Creates transaction and assignes tag to it
 * @param data - transactionCreate input 
 * @param tags - tags to assign
 * @returns transaction with tags
 */
async function createTransactionWithTags(
  data: Transaction.TransactionInput,
  tags: TagObject[]
): Promise<Transaction> {
  console.log(
    `Creating transaction: ${data.amount}, ${data.counter_account_name} with tags: ${tags}`
  );
  return await prisma.transaction.create({
    data: {
      id: data.id,
      account_number: data.account_number,
      due_date: data.due_date,
      amount: data.amount,
      currency: data.currency,
      balance: data.balance,
      counter_account: data.counter_account,
      counter_account_bank_code: data.counter_account_bank_code,
      counter_account_name: data.counter_account_name,
      constant_symbol: data.constant_symbol,
      variable_symbol: data.variable_symbol,
      specific_symbol: data.specific_symbol,
      transaction_type: data.transaction_type,
      note: data.note,
      original_transaction: data.original_transaction,
      rate: data.rate,
      E2E_Identification: data.E2E_Identification,
      original_payer: data.original_payer,
      the_final_beneficiary: data.the_final_beneficiary,
      owner_id: data.owner_id,

      tags: {
        create: Array.from(tags).map((tag) => ({
          tag: {
            connect: {
              id: tag.id
            }
          }
        }))
      }
    }
  });
}
/**
 * Finds all transactions by user ir
 * @param userId - user id
 * @returns array of transactions
 */
async function getTransactionByUserId(userId: string): Promise<Transaction[]> {
  return await prisma.transaction.findMany({
    where: {
      owner_id: userId
    },
    orderBy: {
      due_date: 'asc'
    }
  });
}
/**
 * Finds transactions and tags connected with those transactions by user id
 * @param userId - user id
 * @returns array of transactions with tags connected with it
 */
async function getTransactionsWithTagsByUserId(userId: string): Promise<Transaction[]> {
  return await prisma.transaction.findMany({
    where: {
      owner_id: userId
    },
    include: {
      tags: {
        select: {
          tagId: true
        }
      }
    },
    orderBy: {
      due_date: 'asc'
    }
  });
}
/**
 * Finds first transaction with the specified id
 * @param id - transaction id
 * @returns null/transaction
 */
async function getTransactionByTransactionId(id: number) {
  return await prisma.transaction.findFirst({
    where: {
      id: id
    }
  });
}
/**
 * Finds transaction and its tag by transaction id
 * @param id - transaction id
 * @returns null/transaction + tags connected with it
 */
async function getTransactionWithTagsByTransactionId(id: number) {
  return await prisma.transaction.findFirst({
    where: {
      id: id
    },
    include: {
      tags: {
        select: {
          tagId: true
        }
      }
    }
  });
}
/**
 * Attempts to find transaction with the same properties as given
 * @param owner_id - user id
 * @param amount - transaction.amount
 * @param counter_account - transactiou.counter_account
 * @param due_date - trabsaction.due_date
 * @param constant_symbol  - transaction.constant_symbol
 * @param variable_symbol - transaction.variable_symbol
 * @param specific_symbol -transactions.specific_symbol
 * @returns null/transaction if found
 */
async function getTransactionBySpecified(
  owner_id: string,
  amount: number,
  counter_account: string,
  due_date: Date,
  constant_symbol: string,
  variable_symbol: string,
  specific_symbol: string
) {
  return await prisma.transaction.findFirst({
    where: {
      owner_id,
      amount,
      counter_account,
      due_date,
      constant_symbol,
      specific_symbol,
      variable_symbol
    }
  });
}
/**
 * Finds transaction which are connected with specified tag
 * @param id - tag id
 * @returns all user transactions which are connected with the given tag
 */
async function getTransactionsByTagId(id: number) {
  console.log('Gettings transactions by tag:', id);
  return await prisma.transaction.findMany({
    where: {
      tags: {
        some: {
          tag: {
            id: id
          }
        }
      }
    }
  });
}
/**
 * Deletes transaction by transaction id
 * @param id - transaction id
 * @returns deleted transaction
 */
async function deleteTransaction(id: number) {
  deAssignTagsFromTransactionByTransactionId(id);
  return await prisma.transaction.delete({
    where: {
      id: id
    }
  });
}
/**
 * Deletes all transactions belonging to specified user
 * @param id - user id
 * @returns array of deleted transactions
 */
async function deleteTransactionsByUserId(id: string) {
  return await prisma.transaction.deleteMany({
    where: {
      owner_id: id
    }
  });
}
/**
 * Edits transaction to match the given properties
 * @param id 
 * @param amount 
 * @param counter_account 
 * @param counter_account_name 
 * @param due_date 
 * @param note 
 * @returns edite transaction
 */
async function editTransaction(
  id: number,
  amount: number,
  counter_account: string,
  counter_account_name: string,
  due_date: Date,
  note: string
): Promise<Transaction> {
  console.log('Editing transaction:', id);
  return await prisma.transaction.update({
    where: {
      id: id
    },
    data: {
      amount: amount,
      counter_account: counter_account,
      counter_account_name: counter_account_name,
      due_date: due_date,
      note: note
    }
  });
}
/**
 * Removes all tag-transaction connections from given transaction
 * @param id - transaction id
 * @returns array of transactions whose tag connections has been removed
 */
async function deAssignTagsFromTransactionByTransactionId(id: number) {
  return await prisma.tagOnTransaction.deleteMany({
    where: {
      transactionId: id
    }
  });
}
/**
 * Creates tag
 * @param data Tag creat input (userId, name, color)
 * @returns newly crated tag
 */
async function createTag(data: Tag.TagCreateInput): Promise<Tag> {
  console.log('Creating tag:', data.name);
  return await prisma.tag.create({
    data
  });
}
/**
 * Edits tag to match the give properties
 * @param id - tag id
 * @param tagName 
 * @param tagColor 
 * @returns edited tag
 */
async function editTag(id: number, tagName: string, tagColor: string): Promise<Tag> {
  console.log('Editing tag:', id);
  return await prisma.tag.update({
    where: {
      id: id
    },
    data: {
      name: tagName,
      color: tagColor
    }
  });
}
/**
 * Create connection between the given tag and the transaction
 * @param tagId - tag id
 * @param transactionId - transaction id
 * @returns transaction
 */
async function assignTagToTransaction(tagId: number, transactionId: number) {
  console.log(`Assiging tag ${tagId} to transaction ${transactionId}`);
  return await prisma.transaction.update({
    where: {
      id: transactionId
    },
    data: {
      tags: {
        create: [
          {
            tag: {
              connect: {
                id: tagId
              }
            }
          }
        ]
      }
    }
  });
}
/**
 * Removes the connection between the tag and transaction
 * @param tagId - tag id
 * @param transactionId - transaction id
 * @returns 
 */
async function deAssignTagFromTransaction(tagId: number, transactionId: number) {
  console.log(`Deassiging tag ${tagId} from transaction ${transactionId}`);

  return await prisma.tagOnTransaction.delete({
    where: {
      tagId_transactionId: { tagId, transactionId }
    }
  });
}
/**
 * Removes all connection created with the give tag
 * @param tagId - tag id
 * @returns 
 */
async function deAssignAllTagsByTagId(tagId: number) {
  console.log(`Deassiging tag ${tagId} from all connections`);

  const tags1 = await prisma.tagOnTransaction.deleteMany({
    where: {
      tagId: tagId
    }
  });

  const tags2 = await prisma.tagOnAccount.deleteMany({
    where: {
      tagId: tagId
    }
  });

  return [tags1, tags2];
}

/**
 * Removes tag-account connection of the given account
 * @param accountId - account id
 * @returns account stripped of tags
 */
async function deAssingAllTagsByAccountId(accountId: number) {
  console.log(`Deassiging account ${accountId} from all connections`);

  return await prisma.tagOnAccount.deleteMany({
    where: {
      accountId: accountId
    }
  });
}
/**
 * Finds tags of the given transaction
 * @param id - transaction id
 * @returns array of tags
 */
async function getTagsByTransactionId(id: number): Promise<Tag[]> {
  //console.log('Looking for tags of transaction:', id);
  return await prisma.tag.findMany({
    where: {
      transactions: {
        some: {
          transactionId: id
        }
      }
    }
  });
}
/**
 * Finds all tags of the given user
 * @param id - user id
 * @returns array of tags
 */
async function getTagsByUserId(id: string): Promise<Tag[]> {
  return await prisma.tag.findMany({
    where: {
      owner_id: id
    }
  });
}
/**
 * Finds all accounts of the given user
 * @param id - account id
 * @returns array of accounts
 */
async function getTagsByAccountId(id: number): Promise<Tag[]> {
  return await prisma.tag.findMany({
    where: {
      accounts: {
        some: {
          accountId: id
        }
      }
    }
  });
}
/**
 * Finds tag by id
 * @param id - tag id
 * @returns tag
 */
async function getTagById(id: number) {
  return await prisma.tag.findFirst({
    where: {
      id: id
    }
  });
}
/**
 * Attempts to find current user's tag of the given name
 * @param tagName - name of the tag
 * @param userId - user id
 * @returns null/tag if found
 */
async function getTagByNameAndUserId(tagName: string, userId: string) {
  return await prisma.tag.findFirst({
    where: {
      name: tagName,
      owner_id: userId
    }
  });
}
/**
 * Deletes tag
 * @param id - tag id
 * @returns deleted tag
 */
async function deleteTag(id: number): Promise<Tag> {
  return await prisma.tag.delete({
    where: {
      id: id
    }
  });
}
/**
 * Delete all tags of the given user
 * @param id - user id
 * @returns array of deleted tags
 */
async function deleteTagsByUserId(id: string) {
  return await prisma.tag.deleteMany({
    where: {
      owner_id: id
    }
  });
}
/**
 * Creates account
 * @param data - account create input (userId, name, number)
 * @returns created account
 */
async function createAccount(data: Account.TrasactionCreateInput): Promise<Account> {
  console.log('Creating account:', data.number);
  return await prisma.account.create({
    data
  });
}
/**
 * Creates account and connects it with the given tags
 * @param name 
 * @param number 
 * @param tagIds 
 * @param userId 
 * @returns account + tags connected with it
 */
async function createAccountWithTags(
  name: string,
  number: string,
  tagIds: number[],
  userId: string
) {
  return await prisma.account.create({
    data: {
      name: name,
      number: number,
      owner_id: userId,

      tags: {
        create: tagIds.map(tagId => ({
          tag: {
            connect: {
              id: tagId
            }
          }
        }))
      }
    }
  });
}
/**
 * Edits account to match the given properties
 * @param id
 * @param accountName
 * @param accountNumber 
 * @returns edited account
 */
async function editAccount(
  id: number,
  accountName: string,
  accountNumber: string
): Promise<Account> {
  console.log('Editing account:', id);
  return await prisma.account.update({
    where: {
      id: id
    },
    data: {
      number: accountNumber,
      name: accountName
    }
  });
}
/**
 * Get all accounts of the given user
 * @param id - user id
 * @returns array of accounts
 */
async function getAccountsByUserId(id: string): Promise<Account[]> {
  return await prisma.account.findMany({
    where: {
      owner_id: id
    }
  });
}
/**
 * Finds user accounts and tags which are connected with each account
 * @param id - user id
 * @returns array of accounts + tags connected with the accounts
 */
async function getAccountsWithTagsByUserId(id: string): Promise<Account[]> {
  return await prisma.account.findMany({
    where: {
      owner_id: id
    },
    include: {
      tags: {
        select: {
          tagId: true
        }
      }
    }
  });
}
/**
 * Finds account and tags which are connected with it
 * @param id  account id
 * @returns account + tags connected with it
 */
async function getAccountWithTagsByAccountId(id: number) {
  return await prisma.account.findFirst({
    where: {
      id: id
    },
    include: {
      tags: {
        select: {
          tagId: true
        }
      }
    }
  });
}
/**
 * Attempts to find an account which has the given name and username
 * @param name 
 * @param number 
 * @param userId 
 * @returns null/ account if found
 */
async function getAccountByNameNumberUserId(name: string, number: string, userId: string) {
  return await prisma.account.findFirst({
    where: {
      name: name,
      number: number,
      owner_id: userId
    },
    include: {
      tags: {
        select: {
          tagId: true
        }
      }
    }
  });
}
/**
 * Creates tag-account connection
 * @param tagId - tag id
 * @param accountId - account id
 * @returns 
 */
async function assignTagToAccount(tagId: number, accountId: number) {
  console.log(`Assiging tag ${tagId} to account ${accountId}`);
  return await prisma.account.update({
    where: {
      id: accountId
    },
    data: {
      tags: {
        create: [
          {
            tag: {
              connect: {
                id: tagId
              }
            }
          }
        ]
      }
    }
  });
}
/**
 * Connects multiple tags with the given account
 * @param accountId - account id
 * @param tagIds - array of tag ids
 * @returns account and tags connected with it
 */
async function assignTagsToAccount(accountId: number, tagIds: number[]) {
  console.log(`Assiging tags ${tagIds} to account ${accountId}`);
  return await prisma.account.update({
    where: {
      id: accountId
    },
    data: {
      tags: {
        create: tagIds.map((tagId) => ({
          tag: {
            connect: {
              id: tagId
            }
          }
        }))
      }
    }
  });
}
/**
 * Removes tag-account connection
 * @param tagId - tag id
 * @param accountId - account id
 * @returns 
 */
async function deAssignTagFromAccount(tagId: number, accountId: number) {
  console.log(`Deassiging tag ${tagId} from account ${accountId}`);

  return await prisma.tagOnAccount.delete({
    where: {
      tagId_accountId: { tagId, accountId }
    }
  });
}
/**
 * Deletes account
 * @param id - account id
 * @returns deleted account
 */
async function deleteAccount(id: number) {
  deAssingAllTagsByAccountId(id);
  return await prisma.account.delete({
    where: {
      id: id
    }
  });
}
/**
 * Delete all accounts of the given user
 * @param id - account id
 * @returns array of delted accounts
 */
async function deleteAccountsByUserId(id: string) {
  return await prisma.account.deleteMany({
    where: {
      owner_id: id
    }
  });
}

/**
 * Offers various functions to edit the db
 */
export const db = {
  createUser,
  deleteUser,
  getUserByUsername,
  createTransaction,
  createTransactionWithTags,
  getTransactionByUserId,
  getTransactionsWithTagsByUserId,
  getTransactionByTransactionId,
  getTransactionWithTagsByTransactionId,
  getTransactionBySpecified,
  getTransactionsByTagId,
  editTransaction,
  deleteTransaction,
  deleteTransactionsByUserId,
  createTag,
  editTag,
  assignTagToTransaction,
  deAssignTagFromTransaction,
  deAssignAllTagsByTagId,
  // deAssignAllTagsByTagId2,
  getTagsByTransactionId,
  getTagsByUserId,
  getTagsByAccountId,
  getTagById,
  getTagByNameAndUserId,
  deleteTag,
  deleteTagsByUserId,
  createAccount,
  createAccountWithTags,
  editAccount,
  getAccountsByUserId,
  getAccountsWithTagsByUserId,
  getAccountWithTagsByAccountId,
  getAccountByNameNumberUserId,
  assignTagToAccount,
  assignTagsToAccount,
  deAssignTagFromAccount,
  deleteAccount,
  deleteAccountsByUserId
};
