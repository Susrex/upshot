import { type Actions, fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { db } from '$lib/scripts/db';
import { Encrypt } from '$lib/scripts/hash';
import { auth } from '$lib/server/lucia';
import {defaultAccounts} from '$lib/scripts/defaultAccounts';
import {importAccounts} from '$lib/scripts/importAccounts'

// If the user exists, redirect authenticated users to the profile page.
export const load: PageServerLoad = async ({ locals }) => {
  const session = await locals.validate();
  if (session) throw redirect(302, '/user');
  return {};
};

export const actions: Actions = {
  register: async ({ request, locals }) => {
    const passwordMinLength = 6;
    const data = await request.formData();
    const username: string = data.get('username');
    const password: string = data.get('password');

    if (password.length < passwordMinLength) {
      // fail returns form object - used to giving feedback to the user
      return fail(400, {
        username, // username is returend directly to form - user doesn't have to type it in again
        visible: true,
        error: true,
        message: `Password minimum length is ${passwordMinLength}.`
      });
    }

    //checks if username is unique
    if ((await db.getUserByUsername(username)) == null) {
      const hashWord = await Encrypt.cryptPassword(password);
      const user = await db.createUser({
        username: username,
        password: hashWord
      });

      // imports default accounts
      let accounts = defaultAccounts
      importAccounts(accounts, user.id)

      console.log('Creating session, Userid:', user.id);
      let id = user.id;
      try {
        const session = await auth.createSession(id);
        console.log('Session created successfully');
        locals.setSession(session);
      } catch (e) {
        console.log('Creating session failed due to:');
        console.log(e);
      }

      return { error: false, visible: true, message: `Username:${username} succesfully created.` };
    } else {
      return fail(400, {
        username,
        visible: true,
        error: true,
        message: `The username:${username} has already been taken. Please choose a different one.`
      });
    }
  }
};
