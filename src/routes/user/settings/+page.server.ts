import { type Actions, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

// if the user is not logged in redirecting to login page
export const load: PageServerLoad = async ({ locals, params }) => {
  const { session, user } = await locals.validateUser();
  if (!session) throw redirect(302, '/login');
};

