import { db } from '$lib/scripts/db';
import { CSVtoJson } from '$lib/scripts/papaparse';
import { type Actions, fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { auth } from '$lib/server/lucia';
import { stringToDate, type TransactionObject } from '$lib/scripts/other';

// if the user is not logged in redirecting to login page
// otherwise fetch use transactions
export const load: PageServerLoad = async ({ locals, params }) => {
  const { session, user } = await locals.validateUser();
  if (!session) throw redirect(302, '/login');
  const transactions = db.getTransactionByUserId(user.userId)
  return {
    transactions
  };
};
