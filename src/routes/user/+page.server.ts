import { db } from '$lib/scripts/db';
import { CSVtoJson } from '$lib/scripts/papaparse';
import { type Actions, fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { auth } from '$lib/server/lucia';
import { stringToDate, type TransactionObject, TagObject, AccountObject } from '$lib/scripts/other';
import * as queries from '$lib/scripts/dbQueries';
import { importAccounts } from '$lib/scripts/importAccounts';

export const load: PageServerLoad = async ({ locals, params }) => {
  // validating user
  const { session, user } = await locals.validateUser();
  if (!session) throw redirect(302, '/login');
  //getting user statistics
  const transactions = db.getTransactionByUserId(user.userId);
  const tags = db.getTagsByUserId(user.userId);
  const accounts = db.getAccountsByUserId(user.userId);
  return {
    transactions: transactions,
    tags: tags,
    accounts: accounts
  };
};

export const actions: Actions = {
  signout: async ({ locals }) => {
    const { session, user } = await locals.validateUser();
    if (!session) return fail(401);
    await auth.invalidateAllUserSessions(user.userId); // invalidate all user's sessions
    locals.setSession(null); // remove cookie
  },

  importTransactions: async ({ request, locals }) => {
    const { session, user } = await locals.validateUser();
    if (!session) return fail(401);

    console.log('Importing bank transcript file...');
    const data = await request.formData();
    let transactions = [];

    try {
      // getting file from the file input using the value of 'name' attribute of the input
      const file: File = data.get('bank-transcript');
      transactions = await CSVtoJson(file);
    } catch (e) {
      console.log('Reading file failed due to:');
      console.log(e);
      return fail(400, {
        visible: true,
        error: true,
        message: `Something went wrong while reading the file...`
      });
    }

    let transactionsImported: number = 0;
    let transactionsFailed: number = 0;
    let transactionsExisted: number = 0;
    let tagsAdded: number = 0;

    let accountsWithTags = await queries.getAccountsWithTags(user.userId);

    for (let i = 0; i < transactions.length; i++) {
      let date: Date;
      try {
        date = stringToDate(transactions[i]['due date']);
      } catch (e) {
        console.log(`Importing transaction ${transactions[i]} failed due to:`);
        console.log(e);
        transactionsFailed += 1;
        continue;
      }
      let balance = transactions[i]['balance'] ? transactions[i]['balance'] : 0;
      let amount = transactions[i]['amount'] ? transactions[i]['amount'] : 0;
      // converting imported data into transaction object
      const data: TransactionObject = {
        account_number: transactions[i]['account number'], //bracket notation requried because json keys contains spaces
        due_date: date,
        amount: parseFloat(amount),
        currency: transactions[i]['currency'],
        balance: parseFloat(balance),
        counter_account: transactions[i]['counter account'],
        counter_account_bank_code: transactions[i]['counter account bank code'],
        counter_account_name: transactions[i]['counter account name'],
        constant_symbol: transactions[i]['constant symbol'],
        variable_symbol: transactions[i]['variable symbol'],
        specific_symbol: transactions[i]['specific symbol'],
        transaction_type: transactions[i]['transaction type'],
        note: transactions[i]['note'],
        original_transaction: transactions[i]['original transaction'],
        rate: transactions[i]['rate'],
        E2E_Identification: transactions[i]['E2E Identification'],
        original_payer: transactions[i]['original payer'],
        the_final_beneficiary: transactions[i]['the final beneficiary'],
        owner_id: user.userId
      };
      // checks if transaction already exists if it does it is not imported to the database again
      const transactionExists: TransactionObject | undefined = await db.getTransactionBySpecified(
        data.owner_id,
        data.amount,
        data.counter_account,
        data.due_date,
        data.constant_symbol,
        data.variable_symbol,
        data.specific_symbol
      );
      // finds if the transaction property coutner_account_name matches the name of known accounts
      let matchingAccountName = queries.getMatchingAccountName(
        accountsWithTags,
        data.counter_account_name
      );
      // finds if the transaction property coutner_account matches the number of known accounts
      let matchingAccountNumber = queries.getMatchingAccountNumber(
        accountsWithTags,
        data.counter_account
      );

      if (transactionExists) {
        transactionsExisted += 1;
        // transaction exists, it won't be crated again
        // Update transaction if the accounts name is saved and tags are assigned to it
        const transactionTags = await db.getTagsByTransactionId(transactionExists.id);
        if (matchingAccountName >= 0) {
          let tagsToAdd:TagObject[] = accountsWithTags[matchingAccountName].tags; // array of tags which should be assigned to the transaction
          tagsToAdd.forEach((tag) => {
            // each tag can be assigned to transaction only once, only tags which are not yet assigned may be assigned...
            if (transactionTags.map((e) => e.id).indexOf(tag.id) < 0 && tag) {
              db.assignTagToTransaction(tag.id, transactionExists.id);
              tagsAdded += 1;
              transactionTags.push(tag);
            }
          });
        }
        // Update transaction if the accounts number is saved and tags are assigned to it
        if (matchingAccountNumber >= 0) {
          let tagsToAdd:TagObject = accountsWithTags[matchingAccountNumber].tags; // array of tags which should be assigned to the transaction
          tagsToAdd.forEach((tag) => {
            if (transactionTags.map((e) => e.id).indexOf(tag.id) < 0 && tag) {
              db.assignTagToTransaction(tag.id, transactionExists.id);
              tagsAdded += 1;
              transactionTags.push(tag);
            }
          });
        }
        continue;
      }
      try {
        let tagsToAdd: TagObject[] = [];
        // finding which tags must be assigned to the transactions
        if (matchingAccountName >= 0) {
          tagsToAdd = tagsToAdd.concat(accountsWithTags[matchingAccountName].tags);
        }
        if (matchingAccountNumber >= 0) {
          tagsToAdd = tagsToAdd.concat(accountsWithTags[matchingAccountNumber].tags);
        }
        tagsToAdd = new Set(tagsToAdd); //keeping only unique tags (each tag can be assigned only once)
        tagsToAdd = Array.from(tagsToAdd); //converting back to array to be able to use functions such as map

        if (await db.createTransactionWithTags(data, tagsToAdd)) {
          transactionsImported += 1;
          tagsAdded += tagsToAdd.length;
        }
      } catch (e) {
        transactionsFailed += 1;
        console.log(`Importing transaction ${data} failed due to:`);
        console.log(e);
      }
    }
    const message = `Transactions imported. ${transactionsImported} transactions imported successfully, ${transactionsFailed} transactions failed, ${transactionsExisted} not imported because they were already found in database, ${tagsAdded} tags added automatically`;
    console.log(message);
    let error = transactionsImported + tagsAdded === 0 ? true : false;
    return { error: error, visible: true, message };
  },

  importAccounts: async ({ request, locals }) => {
    const { session, user } = await locals.validateUser();
    if (!session) return fail(401);

    console.log('Importing accounts from file...');
    const data = await request.formData();
    let accounts: any[] = [];

    try {
      if (data.get('account-list') != null) {
        const file: File = data.get('account-list'); // value of 'name' attribute of input
        accounts = await CSVtoJson(file, false);
      }
    } catch (e) {
      console.log('Reading file failed due to:');
      console.log(e);
      return fail(400, {
        visible: true,
        error: true,
        message: `Something went wrong while reading the file...`
      });
    }

    const stats = await importAccounts(accounts, user.userId);
    const message = `Accounts imported. ${stats.accountsCreated} accounts created, ${stats.accountsFoundInDB} accounts already existed, ${stats.tagsCreated} tags created, ${stats.tagsAssigned} tags assigned, encountered ${stats.problemsEncountered} problems during import`;
    console.log(message);
    const error =
      stats.accountsCreated + stats.tagsCreated + stats.tagsAssigned === 0 ? true : false;
    let formAccounts = { error: error, visible: true, message };
    return formAccounts;
  }
};
