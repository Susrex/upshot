import type { PageServerLoad } from './$types';
import { type Actions, fail, redirect } from '@sveltejs/kit';
import { db } from '$lib/scripts/db';
import { Encrypt } from '$lib/scripts/hash';
import { auth } from '$lib/server/lucia';

// If the user exists, redirect authenticated users to the profile page.
export const load: PageServerLoad = async ({ locals }) => {
  const session = await locals.validate();
  if (session) throw redirect(302, '/user');
};

export const actions: Actions = {
  login: async ({ request, locals }) => {
    const data = await request.formData();
    const username: string = data.get('username');
    const password: string = data.get('password');

    console.log('Logging in');

    const user = await db.getUserByUsername(username);
    if (user == null) {
      console.log('User not found');
      return fail(400, {
        visible: true,
        error: true,
        message: `The user:${username} does not exist. You need to register the user:${username} first`
      });
    } else {
      const passwordMatches = await Encrypt.comparePassword(password, user.password);
      if (!passwordMatches) {
        return fail(400, {
          username,
          visible: true,
          error: true,
          message: `The user:${username} does not match the given credentials. Please try again.`
        });
      } else {
        const session = await auth.createSession(user.id);
        locals.setSession(session);
        console.log('Session created');
      }
    }
    return { error: false, visible: true, message: `User:${username} successfully logged in.` };
  }
};
