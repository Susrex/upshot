import { handleServerSession } from '@lucia-auth/sveltekit';
import { type Actions } from '@sveltejs/kit';

export const load = handleServerSession();
