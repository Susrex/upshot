import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';
import type { TagObject } from '$lib/scripts/other';
import * as queries from '$lib/scripts/dbQueries';

/**
 * Fetches user's transactions
 * @param param0 includes user id taken from the route
 * @returns transaction array
 */
export async function GET({ params }) {
  const userId: string = params.id;
  const transactions = await db.getTransactionsWithTagsByUserId(userId);
  if (transactions) {
    return new Response(JSON.stringify(transactions));
  } else {
    throw error(400, 'Failed to fetch transactions from current user');
  }
}

/**
 * Creates transacion with tags
 *  - tags are extracted from user accounts only if the transaction.counter_account or transaction.counter_account_name matches the properties of user accounts
 * @param param0 includes user id taken from the route
 * @returns sucess messege
 */
export async function POST({ params, request }) {
  const userId: string = params.id;
  const transaction = await request.json();
  console.log('creating transaction bla', transaction);

  let accountsWithTags = await queries.getAccountsWithTags(userId);
  let matchingAccountName = queries.getMatchingAccountName(
    accountsWithTags,
    transaction.counter_account_name
  );
  let matchingAccountNumber = queries.getMatchingAccountNumber(
    accountsWithTags,
    transaction.counter_account
  );
  let tagsToAdd: TagObject[] = [];

  if (matchingAccountName >= 0) {
    tagsToAdd = tagsToAdd.concat(accountsWithTags[matchingAccountName].tags);
  }
  if (matchingAccountNumber >= 0) {
    tagsToAdd = tagsToAdd.concat(accountsWithTags[matchingAccountNumber].tags);
  }
  //keeping only unique tags (each tag can be assigned only once), converting back to array to have functions like map...
  tagsToAdd = Array.from(new Set(tagsToAdd));

  const data = {
    amount: parseFloat(transaction.amount),
    owner_id: userId,
    counter_account: transaction.counter_account,
    counter_account_name: transaction.counter_account_name,
    due_date: new Date(transaction.due_date),
    currency: 'CZK',
    note: transaction.note,
    account_number: '',
    balance: 0,
    counter_account_bank_code: '',
    constant_symbol: '',
    variable_symbol: '',
    specific_symbol: '',
    transaction_type: '',
    original_transaction: '',
    rate: '',
    E2E_Identification: '',
    original_payer: '',
    the_final_beneficiary: ''
  };
  const transactionCreated = await db.createTransactionWithTags(data, tagsToAdd);
  if (transactionCreated) {
    return new Response('Transaction created');
  } else {
    throw error(400, 'Failed to create new transaction');
  }
}
