import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';
import { auth } from '$lib/server/lucia';


export async function DELETE({ locals, params }) {
  const userId: string = params.id;
  try {
    db.deleteUser(userId);
    await auth.invalidateAllUserSessions(userId);
    locals.setSession(null); // remove cookie
    return new Response('User account deleted');
  } catch (e) {
    console.log('Deleting user account failed due to:');
    console.log(e);
    throw error(400, 'Failed to delete user account');
  }
}
