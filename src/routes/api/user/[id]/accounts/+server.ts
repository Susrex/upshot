import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches user accounts
 * @param param0 includes user id taken from the route
 * @returns account array
 */
export async function GET({params}) {
    const userId:string = params.id
    const accounts = await db.getAccountsWithTagsByUserId(userId)
    if (accounts) {
        return new Response(JSON.stringify(accounts));
      } else {
        throw error(400, "Failed to fetch user accounts");
      }
}

/**
 * Create account
 * @param param0 includes user id taken from the route
 * @returns success message
 */
export async function POST({params, request}) {
  const userId:string = params.id
  const bodyProperties = await request.json();
  const name = bodyProperties.name
  const number = bodyProperties.number
  const account = await db.createAccount({
    name: name,
    owner_id: userId,
    number: number
  })
  if (account) {
      return new Response("Account created");
    } else {
      throw error(400, "Failed to create account");
    }
}

/**
 * Edits user's account
 * @param param0 includes user id taken from the route
 * @returns edited account
 */
export async function PUT({request}){
  const accountPropertiesToEdit = await request.json();
  const edited = await db.editAccount(
    accountPropertiesToEdit.id,
    accountPropertiesToEdit.name,
    accountPropertiesToEdit.number
  )
  if (edited) {
    return new Response(JSON.stringify(edited))
  }
  else{
    throw error(400, "Editing account failed")
  }
}
