import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches user tags
 * @param param0 includes user id taken from the route
 * @returns tag array
 */
export async function GET({params}) {
    const userId:string = params.id
    const tags = await db.getTagsByUserId(userId)
    if (tags) {
        return new Response(JSON.stringify(tags));
      } else {
        throw error(400, "Failed to fetch transactions from current user");
      }
}

/**
 * Creates user's tag
 * @param param0 includes user id taken from the route
 * @returns sucess message
 */
export async function POST({params, request}) {
  const userId:string = params.id
  const tagProperties = await request.json();
  const name = tagProperties.name
  const color = tagProperties.color
  const tag = await db.createTag({
    name: name,
    owner_id: userId,
    color: color
  })
  if (tag) {
      return new Response("Tag created");
    } else {
      throw error(400, "Failed to fetch transactions from current user");
    }
}

/**
 * Edits tag
 * @param param0 includes user id taken from the route
 * @returns sucess message
 */
export async function PUT({request}){
  const tagPropertiesToEdit = await request.json();
  const edited = await db.editTag(
    tagPropertiesToEdit.id,
    tagPropertiesToEdit.name,
    tagPropertiesToEdit.color
  )
  if (edited) {
    return new Response("Tag edited")
  }
  else{
    throw error(400, "Editing tag failed")
  }
}

/**
 * Deletes tag
 * @param param0 includes user id taken from the route
 * @returns sucess messege
 */
export async function DELETE({request}){
  const tagProperties = await request.json();
  const tagId = tagProperties.id;
  const deletedTagOnTransaction = await db.deAssignAllTagsByTagId(tagId)
  const deleted = await db.deleteTag(tagId);
  if (deleted && deletedTagOnTransaction) {
    return new Response("Tag deleted")
  }
  else{
    throw error(400, "Deleting tag failed")
  }
}
