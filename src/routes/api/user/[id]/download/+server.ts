import * as queries from '$lib/scripts/dbQueries';
import type { AccountObject } from '$lib/scripts/other';
import { convertToCSV } from '$src/lib/scripts/papaparse.js';

/**
 * Creates and returns file with user accounts and tags connected with
 * @param param0 includes user id taken from the route
 * @returns file
 */
export async function GET({ params }) {
  const userId: string = params.id;
  let accountsWithTags: AccountObject[] = await queries.getAccountsWithTags(userId);
  // removing unnecessary data from accounts with tags Object
  // it keeps only name, number, and tags (converted to string)
  accountsWithTags.forEach((account) => {
    delete account.id;
    delete account.owner_id;
    let tagNames = '';
    if (account.tags) {
      account.tags.forEach((tag) => {
        tagNames += tag.name + ',';
      });
    }
    delete account.tags;
    // removing comma at the end of the string
    account.tagNames = tagNames.substring(0, tagNames.length - 1);
  });
  // creates csv file
  let stringAccounts = await convertToCSV(accountsWithTags);
  let blob = new Blob([stringAccounts], { type: 'text/csv;charset=utf-8,' });
  return new Response(blob);
}
