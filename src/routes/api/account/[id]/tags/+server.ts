import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches account tags from the db
 * @param param0 - includes id of the account taken from route
 * @returns array of account tags
 */
export async function GET({params}) {
    const accountId:number = +params.id
    const tags = await db.getTagsByAccountId(accountId)
    if (tags) {
        return new Response(JSON.stringify(tags));
      } else {
        throw error(400, "Failed to fetch transactions from current user");
      }
}

/**
 * Assigns/deassigns tag to/from account
 * @param param0 - includes id of the account taken from route
 * @returns - success message
 */
export async function PUT({params, request}) {
  const accountId:number = +params.id
  const bodyProperties = await request.json();
  const tagId = bodyProperties.tagId;

  let account;
  if(bodyProperties.assign){
    account = await db.assignTagToAccount(tagId, accountId)
  }
  else {
    account = await db.deAssignTagFromAccount(tagId, accountId)
  }
  if (account) {
    return new Response("Tag assigned/deassigned");
  } else {
    throw error(400, "Failed to connect to database");
  }

}
