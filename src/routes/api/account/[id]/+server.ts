import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches user accounts and tags connected to them
  * @param param0 - includes id of the account taken from route
 * @returns accounts with tags array
 */
export async function GET({params}) {
  const accountId: number = +params.id;
  let fetched = await db.getAccountWithTagsByAccountId(accountId);
  if (fetched) {
    return new Response(JSON.stringify(fetched));
  } else {
    throw error(400, "This transaction doesn't exists");
  }
}

/**
 * Deletes account
 * @param param0 - includes id of the account taken from route
 * @returns sucess message
 */
export async function DELETE({ params }) {
  const accountId: number = +params.id;
  let deleted = await db.deleteAccount(accountId);
  if (deleted) {
    return new Response("Account deleted");
  } else {
    throw error(400, "This transaction doesn't exists");
  }
}

/**
 * Edits account
 * @param param0 - id of the account taken from route
 * @returns sucess message
 */
export async function PUT({params, request}){
  const accountId:number = +params.id
  const propertiesToEdit = await request.json();
  const edited = await db.editAccount(
    accountId,
    propertiesToEdit.name,
    propertiesToEdit.number
  )
  if (edited) {
     return new Response("Account edited")
  }
  else{
    throw error(400, "Editing transaction failed")
  }
}
