import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches transactions which are connected with the given tag
 * @param param0 - includes tag id taken from route
 * @returns transaction array
 */
export async function GET({params}) {
    const tagId:number = +params.id
    const transactions = await db.getTransactionsByTagId(tagId)
    if (transactions) {
        return new Response(JSON.stringify(transactions));
      } else {
        throw error(400, "Failed to fetch transactions from current user");
      }
}