import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches tag by its id
* @param param0 - includes tag id taken from route
 * @returns tag
 */
export async function GET({params}) {
    const tagId:number = +params.id
    const tag = await db.getTagById(tagId)
    console.log(tag)
    if (tag) {
        return new Response(JSON.stringify(tag));
      } else {
        throw error(400, "Failed to fetch transactions from current user");
      }
}
