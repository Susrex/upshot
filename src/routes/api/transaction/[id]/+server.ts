import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches transaction by its id
 * @param param0 includes transaction id taken from route
 * @returns transaction
 */
export async function GET({params}) {
  const transactionId: number = +params.id;
  let fetched = await db.getTransactionWithTagsByTransactionId(transactionId);
  if (fetched) {
    return new Response(JSON.stringify(fetched));
  } else {
    throw error(400, "This transaction doesn't exists");
  }
}

/**
 * Deletes transaction by its id
 * @param param0 includes transaction id taken from route
 * @returns success message
 */
export async function DELETE({ params }) {
  const transactionId: number = +params.id;
  let deleted = await db.deleteTransaction(transactionId);
  if (deleted) {
    return new Response("Transaction deleted");
  } else {
    throw error(400, "This transaction doesn't exists");
  }
}

/**
 * Edits transaction
 * @param param0 includes transaction id taken from route
 * @returns sucess message
 */
export async function PUT({params, request}){
  const transactionId:number = +params.id
  const propertiesToEdit = await request.json();
  const edited = await db.editTransaction(
    transactionId,
    propertiesToEdit.amount,
    propertiesToEdit.counter_account,
    propertiesToEdit.counter_account_name,
    propertiesToEdit.due_date,
    propertiesToEdit.note
  )
  if (edited) {
    return new Response("Transaction edited")
  }
  else{
    throw error(400, "Editing transaction failed")
  }
}
