import { db } from '$lib/scripts/db';
import { error } from '@sveltejs/kit';

/**
 * Fetches tags connected with the given transaction
 * @param param0 includes transaction id taken from route
 * @returns tag array
 */
export async function GET({params}) {
    const transactionId:number = +params.id
    const tags = await db.getTagsByTransactionId(transactionId)
    if (tags) {
        return new Response(JSON.stringify(tags));
      } else {
        throw error(400, "Failed to fetch transactions from current user");
      }
}

/**
 * Assigns/deassigns tag to/from transaction
 * @param param0 includes transaction id taken from route
 * @returns sucess message
 */
export async function PUT({params, request}) {
    const transactionId:number = +params.id
    const bodyProperties = await request.json();
    const tagId = bodyProperties.tagId;

    let transaction;
    if(bodyProperties.assign){
      transaction = await db.assignTagToTransaction(tagId, transactionId)
    }
    else {
      transaction = await db.deAssignTagFromTransaction(tagId, transactionId)
    }
    if (transaction) {
      return new Response("Tag assigned/deassigned");
    } else {
      throw error(400, "Failed to connect to database");
    }

  }